//
//  main.m
//  AyeSDKTester
//
//  Created by Bart van den Berg on 01/03/2018.
//  Copyright © 2018 Blue Giraffe Games B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
