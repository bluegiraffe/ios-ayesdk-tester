//
//  AppDelegate.h
//  AyeSDKTester
//
//  Created by Bart van den Berg on 01/03/2018.
//  Copyright © 2018 Blue Giraffe Games B.V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AyeSDK.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

